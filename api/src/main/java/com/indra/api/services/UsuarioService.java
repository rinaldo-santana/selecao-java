
package com.indra.api.services;

import java.util.List;
import java.util.Optional;

import com.indra.api.models.Usuario;
import com.indra.api.repositories.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * UsuarioService
 */
@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    public Optional<List<Usuario>> busqueTodos() {
        List<Usuario> todos = repository.findAll();
        return Optional.of(todos);
    }

    public Optional<Usuario> busquePorId(Long id){
        return repository.findById(id);
    }


    public Optional<Usuario> salve(Usuario usuario) {
        return Optional.of(this.repository.save(usuario));
    }

    public Optional<Usuario> altere(Long id, Usuario usuario) {
       return this.repository.findById(id).map( item -> {
            item.setId(usuario.getId());
            item.setNome(usuario.getNome());
            item.setSenha(usuario.getSenha());
            return this.repository.save(item);
        });
    }

    public Optional<?> excluaPorId(Long id){
        return this.busquePorId(id).map(usuario -> {
            this.repository.delete(usuario);
            return Optional.empty();
        });
    }
}