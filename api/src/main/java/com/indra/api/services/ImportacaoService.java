
package com.indra.api.services;

import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.indra.api.dtos.ColetaConbustivelDTO;
import com.indra.api.models.ArquivoCSVColetaConbustivelsImpl;
import com.indra.api.models.ArquivoCSVFactory;
import com.indra.api.models.Bandeira;
import com.indra.api.models.ColetaCombustivel;
import com.indra.api.models.ColetaConbustivelPojo;
import com.indra.api.models.ColetaDetalhe;
import com.indra.api.models.Estado;
import com.indra.api.models.HistoricoPreco;
import com.indra.api.models.ImportacaoBody;
import com.indra.api.models.Municipio;
import com.indra.api.models.Produto;
import com.indra.api.models.Regiao;
import com.indra.api.models.Revendedor;
import com.indra.api.models.TipoArquivo;
import com.indra.api.models.UnidadeMedida;
import com.indra.api.repositories.BandeiraRepository;
import com.indra.api.repositories.ColetaCombustivelRepository;
import com.indra.api.repositories.ColetaDetalheRepository;
import com.indra.api.repositories.EstadoRepository;
import com.indra.api.repositories.HistoricoPrecoRepository;
import com.indra.api.repositories.MunicipioRepository;
import com.indra.api.repositories.ProdutoRepository;
import com.indra.api.repositories.RegiaoRepository;
import com.indra.api.repositories.RevendedorRepository;
import com.indra.api.repositories.UnidadeMedidaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ArquivoService
 */
@Service
public class ImportacaoService {

    @Autowired
    private RegiaoRepository regiaoRepository;

    @Autowired
    private EstadoRepository estadoRepository;

    @Autowired
    private MunicipioRepository municipioRepository;

    @Autowired
    private BandeiraRepository bandeiraRepository;

    @Autowired
    private RevendedorRepository revendedorRepository;

    @Autowired
    private ProdutoRepository  produtoRepository;

    @Autowired
    private UnidadeMedidaRepository unidadeMedidaRepository;

    @Autowired
    private ColetaCombustivelRepository coletaCombustivelRepository;

    @Autowired
    private ColetaDetalheRepository coletaDetalheRepository;

    @Autowired
    private HistoricoPrecoRepository historicoPrecoRepository;


    private ArquivoCSVColetaConbustivelsImpl csvColetaConbustivel;
    private ImportacaoBody importacaoBody;

    public void processe(ImportacaoBody body) throws Exception {
        
        this.importacaoBody = body;

        TipoArquivo tipo = TipoArquivo.valueOf(importacaoBody.getTipoDoArquivo());

        switch (tipo) {
            case CSVColetaCombustivel: {
                this.processeColetaCombustivel();
                this.graveRetornoProcessamento();
            };
                break;
            default:
                break;
        }
    }

    private void processeColetaCombustivel() throws Exception {
        
        TipoArquivo tipo = TipoArquivo.valueOf(this.importacaoBody.getTipoDoArquivo());  
        String arquivo = this.importacaoBody.getNomeDoArquivo();
        
        Path path = Paths.get(arquivo);
        if (Files.notExists(path, LinkOption.NOFOLLOW_LINKS)) {
            throw new Exception("Arquivo não existe.");
        }

        csvColetaConbustivel = (ArquivoCSVColetaConbustivelsImpl) 
            new ArquivoCSVFactory().crie(tipo); 

        this.csvColetaConbustivel.processe(arquivo);
    }

    private void graveRetornoProcessamento(){

        List<ColetaConbustivelPojo> lista =  this.csvColetaConbustivel.getLista();
        
        ColetaConbustivelDTO dto = new ColetaConbustivelDTO();

        System.out.println("total da lista " + lista.size());

        lista.stream().forEach(item -> {

           dto.toDTO(item);

           System.out.println();

            Regiao r = regiaoRepository.findBySigla(item.getRegiao());
            r = r == null ? regiaoRepository.save(dto.toRegiao()) : r;

            Estado e = estadoRepository.findBySigla(item.getEstado());
            if (e == null) {
                e = dto.toEstado();
                e.setRegiao(r);
                e = estadoRepository.save(e); 
            }

            Municipio m = municipioRepository.findByNome(item.getMunicipio());
            if (m == null) {
                m = dto.toMunicipio();
                m.setEstado(e);
                m = municipioRepository.save(m);
            }

            Bandeira b = bandeiraRepository.findByNome(item.getBandeira());
            b = b == null ? bandeiraRepository.save(dto.toBandeira()) : b;

            Revendedor rv = revendedorRepository.findByNome(item.getRevenda());
            if (rv == null) {
                rv = dto.toRevendedor();
                rv.setMunicipio(m);
                rv.setBandeira(b);
                rv = revendedorRepository.save(rv);
            }

            UnidadeMedida u = unidadeMedidaRepository.findByNome(item.getUnidadeDeMedida());
            u = u == null ? unidadeMedidaRepository.save(dto.toUnidadeMedida()) : u;
          
            Produto p = produtoRepository.findByNome(item.getProduto());
            if (p == null) {
                p = dto.toProduto();
                p.setUnidadeMedida(u);
                p = produtoRepository.save(p);
            }

            ColetaCombustivel c = dto.toColeta();
            c.setRevendedor(rv);
            c = coletaCombustivelRepository.save(c);

            ColetaDetalhe d = dto.toDetalhe();
            d.setProduto(p);
            d.setColeta(c);
            d = coletaDetalheRepository.save(d);
     
            HistoricoPreco h = dto.toHistoricoPreco();
            if (!BigDecimal.ZERO.equals(h.getPreco())) {
                h.setData(c.getData());
                h.setProduto(p);
                h = historicoPrecoRepository.save(h);
            }
        });  
    }  
}