package com.indra.api.services;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.indra.api.models.HistoricoPreco;
import com.indra.api.models.HistoricoPrecoBody;
import com.indra.api.models.Produto;
import com.indra.api.repositories.HistoricoPrecoRepository;
import com.indra.api.repositories.ProdutoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * HistoricoPrecoService
 */
@Service
public class HistoricoPrecoService {

    @Autowired
    private HistoricoPrecoRepository repository;

    @Autowired
    private ProdutoRepository produtoRepository;
    

    public Optional<List<HistoricoPreco>> busqueTodos() {
        List<HistoricoPreco> todos = repository.findAll();
        return Optional.of(todos);
    }

    public Optional<HistoricoPreco> busquePorId(Long id){
        return repository.findById(id);
    }


    public Optional<HistoricoPreco> salve(HistoricoPrecoBody historico) {
       
        Produto p = this.produtoRepository.findByNome(historico.getNomeDoproduto());
        String[] data = historico.getDataDoHistorico().split("/");
        HistoricoPreco h = new HistoricoPreco();
        h.setData(Date.from( 
            LocalDateTime.of( Integer.parseInt(data[2]), 
                              Integer.parseInt(data[1]), 
                              Integer.parseInt(data[0]),
                              0, 0, 0)
            .atZone(ZoneId.systemDefault())
            .toInstant()));
        h.setPreco(new BigDecimal(historico.getPrecoDoProduto()));
        h.setProduto(p);
        return Optional.of(this.repository.save(h));
    }

    public Optional<HistoricoPreco> altere(Long id, HistoricoPrecoBody historico) {
      
        Produto p = this.produtoRepository.findByNome(historico.getNomeDoproduto());

        return this.repository.findById(id).map( item -> {
            item.setId(historico.getId());
            item.setPreco(new BigDecimal(historico.getPrecoDoProduto()));
            item.setProduto(p);
            return this.repository.save(item);
        });
    }

    public Optional<?> excluaPorId(Long id){
        return this.busquePorId(id).map(historico -> {
            this.repository.delete(historico);
            return Optional.empty();
        });
    }

}