package com.indra.api.models;

import lombok.Data;

/**
 * FonteDeDados
 */
@Data
public class ColetaConbustivelPojo {

    private String regiao;
    private String estado;
    private String municipio;
    private String revenda;
    private String CodigoInstalacao;
    private String produto;
    private String dataDaColeta;
    private String valorDeCompra;
    private String ValorDeVenda;
    private String unidadeDeMedida;
    private String bandeira;

    public ColetaConbustivelPojo(String[] dados){
        
        for (int i = 0; i < dados.length; i++) {
            dados[i] = dados[i].trim();
        }
        this.regiao = dados[0];
        this.estado = dados[1];
        this.municipio = dados[2];
        this.revenda = dados[3];
        this.CodigoInstalacao = dados[4];
        this.produto = dados[5];
        this.dataDaColeta = dados[6];
        this.valorDeCompra = dados[7];
        this.ValorDeVenda = dados[8];
        this.unidadeDeMedida = dados[9];
        this.bandeira = dados[10];
    }
}