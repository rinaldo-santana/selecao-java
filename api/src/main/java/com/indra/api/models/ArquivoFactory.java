package com.indra.api.models;

/**
 * ArquivoFactory
 */
public interface ArquivoFactory {

    Arquivo crie(TipoArquivo tipo);
}