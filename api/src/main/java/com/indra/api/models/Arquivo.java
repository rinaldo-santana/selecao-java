package com.indra.api.models;

import lombok.Getter;

@Getter
public abstract class Arquivo {

    private TipoArquivo tipo;

    public Arquivo(TipoArquivo tipo) {
        this.tipo = tipo;
    }

    protected abstract void processa() throws Exception;
}
