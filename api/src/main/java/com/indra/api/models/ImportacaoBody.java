package com.indra.api.models;

import lombok.Data;

/**
 * ArquivoRequestBody
 */
@Data
public class ImportacaoBody {

    private String tipoDoArquivo;
    private String nomeDoArquivo;
}