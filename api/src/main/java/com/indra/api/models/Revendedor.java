package com.indra.api.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * Classe que representa um revendedor de conbustivel
 */
@Data
@Table
@Entity
public class Revendedor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String nome;
    
    @ManyToOne
    private Municipio municipio;

    private String codigoInstalacao;

    @ManyToOne
    private Bandeira bandeira;
}