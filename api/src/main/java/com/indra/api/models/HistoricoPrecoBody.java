package com.indra.api.models;

import lombok.Data;

/**
 * HistoricoPrecoBody
 */
@Data
public class HistoricoPrecoBody {

    private Long id;
    private String dataDoHistorico;
    private String nomeDoproduto;
    private String precoDoProduto;
}