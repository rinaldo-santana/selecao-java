package com.indra.api.models;

import lombok.Getter;

@Getter
public enum TipoArquivo {
    
    CSVColetaCombustivel("CSVColetaCombustivel");

    private String descricao;
   
    private TipoArquivo(String descricao){
        this.descricao = descricao;
    }
}
