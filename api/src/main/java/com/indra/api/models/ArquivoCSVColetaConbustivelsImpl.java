package com.indra.api.models;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * ArquivoCSVImpl
 */
public class ArquivoCSVColetaConbustivelsImpl extends Arquivo {

    private String arquivo;
    private List<ColetaConbustivelPojo> listaColetaConbustivels = new ArrayList<>();

    public ArquivoCSVColetaConbustivelsImpl() {
        super(TipoArquivo.CSVColetaCombustivel);
    }

    @Override
    protected void processa() throws Exception {

        try (BufferedReader csvReader = new BufferedReader(new FileReader(this.arquivo))) {

            csvReader.lines().skip(1).forEach(linha -> {
                linha = linha.trim().replace("\"", "");
                String[] line = linha.split("  ");
                
                this.listaColetaConbustivels.add(new ColetaConbustivelPojo(line));
            });
        } catch (Exception e) {
           throw e;
        }
    }

    public void processe(String arquivo) throws Exception {
        this.arquivo = arquivo;
        this.processa();
    }


    public List<ColetaConbustivelPojo> getLista() {
        return this.listaColetaConbustivels;
    }

  
}