
package com.indra.api.models;

/**
 * ArquivoCSVFactory
 */
public class ArquivoCSVFactory implements ArquivoFactory{

    @Override
    public Arquivo crie(TipoArquivo tipo) {
        switch (tipo) {
            case CSVColetaCombustivel: return new ArquivoCSVColetaConbustivelsImpl();
        default:
            return null;
        }  
    }
    
}