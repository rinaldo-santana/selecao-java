
package com.indra.api.models;


import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * ColetaDetalhes
 */
@Table
@Entity
@Data
public class ColetaDetalhe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
  
    @ManyToOne
    private ColetaCombustivel coleta;

    @OneToOne
    private Produto produto;

    private BigDecimal valorCompra;
}