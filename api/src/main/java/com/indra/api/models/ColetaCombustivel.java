package com.indra.api.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

/**
 * Classe que representa uma coleta Coleta de produtos 
 * feita por um Revendedor
 */
@Data
@Table
@Entity
public class ColetaCombustivel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.DATE)
    private Date data;

    @OneToOne
    private Revendedor revendedor;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "coleta")
    private List<ColetaDetalhe> detalhes = new ArrayList<>();
    
}