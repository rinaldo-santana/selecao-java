
package com.indra.api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * SwaggerConfig
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
          .select()
          .apis(RequestHandlerSelectors.basePackage("com.indra.api.controllers"))
          .paths(PathSelectors.ant("/api/*"))
          .build()
          .apiInfo(this.apiInfo())
          .useDefaultResponseMessages(true);
    }

    
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("API Selecao Java")
                .description("API desenvolvida em desafio protosto pela Indra")
                .version("1.0.0")
                .contact(new Contact("Rinaldo Santana", "https://github.com/rinaldo-santana", "rinaldo.rms@gmail.com"))
                .build();
    }

}

