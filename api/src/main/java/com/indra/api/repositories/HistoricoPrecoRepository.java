package com.indra.api.repositories;

import com.indra.api.models.HistoricoPreco;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * HistoricoPrecoRepository
 */
public interface HistoricoPrecoRepository extends JpaRepository<HistoricoPreco, Long>{

    
}