
package com.indra.api.repositories;

import com.indra.api.models.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * UsuarioRepository
 */
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}