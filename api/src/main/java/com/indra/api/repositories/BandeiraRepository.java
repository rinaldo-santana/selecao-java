package com.indra.api.repositories;

import com.indra.api.models.Bandeira;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * BandeiraRepository
 */
public interface BandeiraRepository  extends JpaRepository<Bandeira, Long>{

    Bandeira findByNome(String nome);
    
}