package com.indra.api.repositories;

import com.indra.api.models.ColetaCombustivel;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * ColetaCombustivelRepository
 */
public interface ColetaCombustivelRepository extends JpaRepository<ColetaCombustivel, Long> {


}