package com.indra.api.repositories;

import com.indra.api.models.Municipio;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MunicipioRepository extends JpaRepository<Municipio, Long>{

    Municipio findByNome(String nome);
}
