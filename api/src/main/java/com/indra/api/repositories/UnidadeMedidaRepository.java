
package com.indra.api.repositories;

import com.indra.api.models.UnidadeMedida;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * UnidadeMedidaRepository
 */
public interface UnidadeMedidaRepository extends JpaRepository<UnidadeMedida, Long> {
   
    UnidadeMedida findByNome(String nome);
    
}