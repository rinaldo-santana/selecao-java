
package com.indra.api.repositories;

import com.indra.api.models.Regiao;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * RegiaoRepository
 */
public interface RegiaoRepository extends JpaRepository<Regiao, Long> {
    
    Regiao findBySigla(String sigla);
}