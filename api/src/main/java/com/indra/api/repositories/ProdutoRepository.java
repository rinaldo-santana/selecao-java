package com.indra.api.repositories;

import com.indra.api.models.Produto;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * ProdutoRepository
 */
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

        Produto findByNome(String nome);
    
}