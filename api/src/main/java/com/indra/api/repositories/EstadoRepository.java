
package com.indra.api.repositories;

import com.indra.api.models.Estado;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * EstadoRepository
 */
public interface EstadoRepository extends JpaRepository<Estado, Long> {
    
    Estado findBySigla(String sigla);
}