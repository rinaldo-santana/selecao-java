package com.indra.api.repositories;

import com.indra.api.models.ColetaDetalhe;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * ColetaDetalheRepository
 */
public interface ColetaDetalheRepository extends JpaRepository<ColetaDetalhe, Long>{

}