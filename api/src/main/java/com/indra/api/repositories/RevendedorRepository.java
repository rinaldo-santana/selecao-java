package com.indra.api.repositories;

import com.indra.api.models.Revendedor;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * RevendedorRepository
 */
public interface RevendedorRepository extends JpaRepository<Revendedor, Long>{

    Revendedor findByNome(String nome);
}