
package com.indra.api.controllers;

import java.util.List;

import com.indra.api.models.Usuario;
import com.indra.api.services.UsuarioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * UsuarioController
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @GetMapping()
    public ResponseEntity<List<Usuario>> buscarTodos() {
       return service.busqueTodos().map(resultado -> {
            return ResponseEntity.ok(resultado);
        }).orElse(ResponseEntity.badRequest().build()) ;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Usuario> buscarPor(@PathVariable Long id){
       return service.busquePorId(id).map(resultado -> {
           return ResponseEntity.ok(resultado);
        }).orElse(ResponseEntity.badRequest().build());
    }

    @PostMapping
    public ResponseEntity<Usuario> salvar(@RequestBody Usuario usuario){
      return  this.service.salve(usuario).map(resultado -> {
               return ResponseEntity.ok(resultado); 
        }).orElse(ResponseEntity.badRequest().build());
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Usuario> alterar(@PathVariable Long id, @RequestBody Usuario usuario){
        return this.service.altere(id, usuario).map(resultado -> {
            return ResponseEntity.ok(resultado);
        }).orElse(ResponseEntity.badRequest().build());
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> excluir(@PathVariable Long id){
        return this.service.excluaPorId(id).map( resultado -> {
           return ResponseEntity.ok().build();
        }).orElse(ResponseEntity.badRequest().build());    
    
    }

}