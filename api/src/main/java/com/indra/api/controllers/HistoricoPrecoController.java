package com.indra.api.controllers;

import java.util.List;

import com.indra.api.models.HistoricoPreco;
import com.indra.api.models.HistoricoPrecoBody;
import com.indra.api.services.HistoricoPrecoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * HistoricoPrecoController
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/historicos")
public class HistoricoPrecoController {

    @Autowired
    private HistoricoPrecoService service;

    @GetMapping
    public ResponseEntity<List<HistoricoPreco>> buscarTodos() {
       return service.busqueTodos().map(resultado -> {
            return ResponseEntity.ok(resultado);
        }).orElse(ResponseEntity.badRequest().build()) ;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<HistoricoPreco> buscarPor(@PathVariable Long id){
       return service.busquePorId(id).map(resultado -> {
           return ResponseEntity.ok(resultado);
        }).orElse(ResponseEntity.badRequest().build());
    }

    @PostMapping
    public ResponseEntity<HistoricoPreco> salvar(@RequestBody HistoricoPrecoBody historico){
      return  this.service.salve(historico).map(resultado -> {
               return ResponseEntity.ok(resultado); 
        }).orElse(ResponseEntity.badRequest().build());
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<HistoricoPreco> alterar(@PathVariable Long id, @RequestBody HistoricoPrecoBody historico){
        return this.service.altere(id, historico).map(resultado -> {
            return ResponseEntity.ok(resultado);
        }).orElse(ResponseEntity.badRequest().build());
    }
    

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> excluir(@PathVariable Long id){
        return this.service.excluaPorId(id).map( resultado -> {
           return ResponseEntity.ok().build();
        }).orElse(ResponseEntity.badRequest().build());    
    
    }
}