
package com.indra.api.controllers;

import com.indra.api.models.ImportacaoBody;
import com.indra.api.services.ImportacaoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ProcessadorArquivosController
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/importacao")
public class ImportacaoController {

    @Autowired
    private ImportacaoService service;

    @PostMapping
    public ResponseEntity<?> importarArquivo(@RequestBody ImportacaoBody body) {
        try {
            this.service.processe(body);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
 
    }
}