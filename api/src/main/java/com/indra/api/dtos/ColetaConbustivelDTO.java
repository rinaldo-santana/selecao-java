package com.indra.api.dtos;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;

import com.indra.api.models.Bandeira;
import com.indra.api.models.ColetaCombustivel;
import com.indra.api.models.ColetaConbustivelPojo;
import com.indra.api.models.ColetaDetalhe;
import com.indra.api.models.Estado;
import com.indra.api.models.HistoricoPreco;
import com.indra.api.models.Municipio;
import com.indra.api.models.Produto;
import com.indra.api.models.Regiao;
import com.indra.api.models.Revendedor;
import com.indra.api.models.UnidadeMedida;

/**
 * ColetaDTO
 */
public class ColetaConbustivelDTO {

    private ColetaConbustivelPojo coleta;

    public void toDTO(ColetaConbustivelPojo coleta) {
        this.coleta = coleta;
    }

    public Regiao toRegiao() {
        Regiao regiao = new Regiao();
        regiao.setSigla(this.coleta.getRegiao());

        return regiao;
    }

    public Estado toEstado() {
        Estado estado = new Estado();
        estado.setSigla(this.coleta.getEstado());

        return estado;
    }

    public Municipio toMunicipio() {
        Municipio municipio = new Municipio();
        municipio.setNome(this.coleta.getMunicipio());
        municipio.setEstado(this.toEstado());

        return municipio;
    }

    public Revendedor toRevendedor() {
        Revendedor revendedor = new Revendedor();
        revendedor.setNome(this.coleta.getRevenda());
        revendedor.setMunicipio(this.toMunicipio());
        revendedor.setCodigoInstalacao(this.coleta.getCodigoInstalacao());

        return revendedor;
    }

    public UnidadeMedida toUnidadeMedida() {
        UnidadeMedida unidade = new UnidadeMedida();
        unidade.setNome(this.coleta.getUnidadeDeMedida());

        return unidade;
    }

    public Produto toProduto() {
        Produto produto = new Produto();
        produto.setNome(this.coleta.getProduto());
        produto.setNome(this.coleta.getProduto());
        produto.setPrecoVenda(new BigDecimal(
            this.coleta.getValorDeVenda()
                .replaceFirst(",", ".")
                .replace("", "0") ));
        produto.setUnidadeMedida(this.toUnidadeMedida());

        return produto;
    }

    public Bandeira toBandeira() {
        Bandeira bandeira = new Bandeira();
        bandeira.setNome(this.coleta.getBandeira());
        return bandeira;

    }

    public ColetaDetalhe toDetalhe() {
        ColetaDetalhe detalhe = new ColetaDetalhe();
        detalhe.setColeta(this.toColeta());
        detalhe.setProduto(this.toProduto());
        detalhe.setValorCompra(new BigDecimal(
                    this.coleta.getValorDeCompra()
                               .replaceFirst(",", ".")
                               .replace("", "0")));
        
        return detalhe;
    }

    public ColetaCombustivel toColeta() {

        ColetaCombustivel coleta = new ColetaCombustivel();
        String[] data = this.coleta.getDataDaColeta().split("/");
        coleta.setData(Date.from( 
            LocalDateTime.of( Integer.parseInt(data[2]), 
                              Integer.parseInt(data[1]), 
                              Integer.parseInt(data[0]),
                              0, 0, 0)
            .atZone(ZoneId.systemDefault())
            .toInstant()));
        coleta.setRevendedor(this.toRevendedor());
        
        return coleta;

    }

    public HistoricoPreco toHistoricoPreco() {
        HistoricoPreco historico = new HistoricoPreco();
        if ("".equals(coleta.getValorDeVenda().trim())) {
            historico.setPreco(BigDecimal.ZERO); 
        } else {
            historico.setPreco(new BigDecimal(
                this.coleta.getValorDeVenda().replaceFirst(",", ".")));
        }

        return historico;
    }
}